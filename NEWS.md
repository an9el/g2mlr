# g2mlr 0.1.0

* Added new function to extract the data after apply
pre-processing with mlr3
* Added a `NEWS.md` file to track changes to the package.
* Added a toy data and a function to create toy data
* Added `gitlab-ci.yml`
* Added function to create the task `mlr_create_task`
* Added function to create the learner `mlr_create_lrn`
* Added `mlr_fs_wrapper` as toy example of how to perform feature
selection with `mlr3FSelect`package, with wrapper and nested
crossvalidation
* Added `mlr_fs_filter` as toy example of how to perform feature
selection with `mlr3filters`
* Add a file to load data for the pipeline of the VT project
